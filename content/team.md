---
title: "Team"
date: 2021-06-07T23:20:23+10:00
draft: false
Callout: "These folk wanted you to know they build Serpent OS."
---

The Core Team is responsible for the every-day running and development of Serpent OS.
Together they coordinate milestones, releases and project goals, and implement them
as required. While each contributor will have a specific domain of interest or expertise,
they share the workload in building the core of Serpent OS.


{{<render_team "core">}}

#### Our Loving Community

Most development discussions happen on our matrix channels [#serpentos](https://matrix.to/#/#serpentos:matrix.org) on
matrix.org or in our development channel, [#serpentos-dev](https://matrix.to/#/#serpentos-dev:matrix.org) on matrix.org.

If your matrix client supports spaces (app.element.io does), you can join the [Serpent OS matrix Space](https://matrix.to/#/!trFJOzhpDUejJKnPYg:matrix.org )
which will show all the channels offered by us.

You can also join us at [GitLab](https://gitlab.com/serpent-os) where you'll find all our code, tools and packaging
repositories. This is where users contribute and improve Serpent OS.
