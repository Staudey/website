---
title: "Download"
date: 2022-01-04T17:10:00+01:00
draft: false
Callout: "Grab them ISOs"
---

## Aw, shucks

Looks like we haven't released any ISOs yet!

We're working towards our [0.0 milestone](https://gitlab.com/groups/serpent-os/-/milestones/1#tab-issues),
where we plan to release a fully functioning and self-hosting Serpent OS core system distributed as a
[systemd-nspawn container](https://www.freedesktop.org/software/systemd/man/systemd-nspawn.html).

This container will enable interested parties to start developing Serpent OS packages, 
leveraging our `stone.yml` package recipe format and `boulder` build tooling.

Furthermore, this container will come with support for remote moss index repositories,
which are used for manipulating package selections via our `moss` software lifecycle management tool.

Stay tuned!

{{<download_image>}}
